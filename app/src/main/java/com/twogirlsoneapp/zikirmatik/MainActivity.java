package com.twogirlsoneapp.zikirmatik;

import android.content.Context;
import android.graphics.Typeface;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tx, txZikirHint;
    View btnZikir,btnReset;
    Button btnResett;
    int counter;

    boolean isVibrationOn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         tx = (TextView)findViewById(R.id.tvZikir);
        txZikirHint= findViewById(R.id.txZikirHint);
        setTypeFace();
        btnZikir= findViewById(R.id.btnZikir);

        btnReset= findViewById(R.id.btnReset);
        isVibrationOn=true;
        counter=0;
        tx.setText(Integer.toString(counter));


        btnZikir.setOnClickListener(this);
        btnReset.setOnClickListener(this);



    }


   void setTypeFace(){

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "font/digitalnew.otf");
        tx.setTypeface(custom_font);
       txZikirHint.setTypeface(custom_font);
    }

    @Override
    public void onClick(View v) {
        if (v==btnZikir){
            counter++;
            tx.setText(Integer.toString(counter));

            if (isVibrationOn){
                Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibe.vibrate(150);
            }

        }
        else if (v==btnReset){
            counter=0;
            tx.setText(Integer.toString(counter));


        }
    }
}
